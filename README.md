# PHP-FPM Docker Image

This is a base PHP-FPM image. Use it in conjunction with novadin/nginx and mysql images.

## Usage

The application files should be attached as a volume from a separate container.

```
docker run -d --link mysql:mysql --volumes-from <app container> --name php-fpm novadin/php-fpm
```