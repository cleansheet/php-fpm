FROM debian:stretch-slim

MAINTAINER Andras Foldesi <andras.foldesi@novadin.com>

RUN apt-get update && \
    apt-get install -y php7.0-fpm && \
    apt-get install -y php7.0-mysql && \
    apt-get install -y php7.0-cli && \
    apt-get install -y php7.0-curl && \
    apt-get install -y php7.0-gd && \
    apt-get install -y php7.0-mbstring && \
    apt-get install -y php7.0-xml && \
    apt-get install -y php7.0-zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir /run/php && \
    mkdir /var/log/php-fpm && \
    sed -i 's/;daemonize = yes/daemonize = no/' /etc/php/7.0/fpm/php-fpm.conf && \
    sed -i "s/error_log = \/var\/log\/php7.0-fpm.log/error_log = \/var\/log\/php-fpm\/error.log/" /etc/php/7.0/fpm/php-fpm.conf && \
    sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.0/fpm/php.ini && \
    sed -i "s/;session.cookie_secure =/session.cookie_secure = 1/" /etc/php/7.0/fpm/php.ini && \
    sed -i "s/listen = \/run\/php\/php7.0-fpm.sock/listen = 9000/" /etc/php/7.0/fpm/pool.d/www.conf

COPY wait-for-it.sh /usr/bin/

CMD ["php-fpm7.0"]

EXPOSE 9000
